" set options
set nocompatible   "Vi互換モードの無効化
set encoding=utf-8 "デフォルトエンコーディングを UTF-8に
set autoindent     "自動インデント
set number         "行番号表示
set incsearch      "インクリメンタルサーチ
set hlsearch       "検索結果のハイライト
"set showmode       "現在のモードを表示 (lightlineで表示してるので無効化
set showmatch      "閉じ括弧に対応する括弧をハイライト
set matchtime=1    "showmatchでハイライトする時間 ( 0.1 x n秒
set nowrap         "行の折り返しをしない
set t_Co=256       "256色表示
set shiftwidth=4   "自動インデントに使われる空白の数
set smarttab       "<Tab>の代わりに shiftwidth使う
set expandtab      "InsertModeで<Tab>を挿入するのに適切な数の空白を使う
set tabstop=4      "ファイル内の<Tab>が対応する空白の数
set nobackup       "バックアップファイルを作成しない (バージョン管理してるだろ？
set clipboard=unnamed,autoselect "クリップボード有効化
set display=lastline "一行が長い場合でも全て描画
set pumheight=10   "入力補完時のウィンドウの最大リスト数

" 括弧を入力した時に、キャレットを真ん中へ移動
imap () ()<Left>
imap [] []<Left>
imap {} {}<Left>
imap <> <><Left>
imap "" ""<Left>
imap '' ''<Left>

" remap keys
" キャレットから行末までをヤンク
nnoremap Y y$
" インクリメント、デクリメント
nnoremap + <C-a>
nnoremap - <C-x>

syntax on "構文ハイライトの有効化

" deinの設定
if &compatible
  set nocompatible
endif

let s:dein_dir = $HOME . '/.vim/dein'
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

let &runtimepath = s:dein_repo_dir .','. &runtimepath

call dein#begin(s:dein_dir)

call dein#add(s:dein_repo_dir)
call dein#add('Shougo/vimproc.vim', {'build' : 'make'})
call dein#add('Shougo/vimshell.vim')
call dein#add('Shougo/unite.vim')
call dein#add('Shougo/unite-outline')
call dein#add('Shougo/neocomplete.vim')
call dein#add('Shougo/neosnippet.vim')
call dein#add('Shougo/neosnippet-snippets')
call dein#add('scrooloose/syntastic')
call dein#add('nathanaelkane/vim-indent-guides')
call dein#add('bronson/vim-trailing-whitespace')
call dein#add('itchyny/lightline.vim')
call dein#add('thinca/vim-quickrun')
" colorschemas
call dein#add('tomasr/molokai')
call dein#add('nanotech/jellybeans.vim')
call dein#add('vim-scripts/newspaper.vim')
call dein#add('vim-scripts/Zenburn')
call dein#add('vim-scripts/pyte')
call dein#add('w0ng/vim-hybrid')


" 現在のウィンドウの下に展開
"set splitbelow
"noremap <C-S> :10split<CR>:VimShell<CR>
" 現在のウィンドウの右に展開
"set splitright
"noremap <C-S> :60vsplit<CR>:VimShell<CR>

let g:unite_data_directory = "~/.vim/.unite"
let g:unite_split_rule = "botright"
noremap <C-N> :Unite file<CR>

let g:neocomplete#enable_at_startup = 1

let g:syntastic_enable_signs = 1
let g:syntastic_auto_loc_list = 2

let g:indent_guides_enable_on_vim_startup = 1
:set ts=2 sw=2 et

:set laststatus=2
let g:lightline = { 'colorscheme': 'jellybeans' }

let g:quickrun_config = {
\    "_" : {
\        "outputter/buffer/split" : "botright",
\        "outputter/buffer/close_on_empty" : 1,
\        "runner" : "vimproc",
\        "runner/vimproc/updatetime" : 60
\    },
\}

if filereadable(expand('~/.vimrc.local'))
  source ~/.vimrc.local
endif

let g:hybrid_use_Xresources = 1

call dein#end()

if has('vim_starting') && dein#check_install()
  call dein#install()
endif

filetype plugin indent on

:colorscheme jellybeans
