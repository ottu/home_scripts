output = `cvt 1050 1830`.split()
si = output.index('Modeline') + 1
ei = output.length
modeline = output[si..ei]
resolution = modeline[0]
modeline = modeline.join(' ')

newmode = "xrandr --newmode #{modeline}"
addmode = "xrandr --addmode VNC-0 #{resolution}"
setmode = "xrandr --output VNC-0 --mode #{resolution}"

system( newmode )
system( addmode )
system( setmode )
