#! /usr/bin/sh

cd ~/

echo 'checking ~/.go directory'
if [ ! -d $HOME/.go ]; then
    mkdir $HOME/.go
fi

echo 'export GOPATH'
export GOPATH=$HOME/.go

echo 'install some packages'
sudo pacman -S zsh vim terminator openssh net-tools go git powerline-fonts

echo 'get oh-my-zsh'
curl -L http://install.ohmyz.sh | sh

echo 'get neobundle'
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh

echo 'get dein'
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > dein_installer.sh
sh dein_installer.sh $HOME/.vim/dein
rm dein_installer.sh

echo 'get peco'
go get github.com/peco/peco/cmd/peco

echo 'get enhancd'
if [ ! -d $HOME/.enhancd/src ]; then
    git clone https://github.com/b4b4r07/enhancd $HOME/.enhancd/src
fi

echo 'copy config files'
cp $HOME/Programming/home_scripts/.config/terminator/config $HOME/.config/terminator/
cp $HOME/Programming/home_scripts/.ssh/config $HOME/.ssh/config
