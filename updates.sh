#!/usr/bin/zsh

current=$(cd $(dirname $0); pwd)

user=$1
if [ !user ]; then
    user=`whoami`
fi
echo "exec user is $user."

home=/home/$user

cd $home/.vim/dein/repos/github.com/Shougo/dein.vim
git pull

cd $home/.enhancd/src
git pull

cp $current/vim/.vimrc $home
vim_file=$current/vim/`hostname`
if [ -e $vim_file ]; then
    echo "$vim_file found."
    cp $vim_file $home/.vimrc.local
fi

cp $current/zsh/.zshrc $home
zsh_file=$current/zsh/`hostname`
if [ -e $zsh_file ]; then
    echo "$zsh_file found."
    cp $zsh_file $home/.zshrc.local

    zsh_theme=$(cat $zsh_file | grep ZSH_THEME)
    if [ $zsh_theme ]; then
        echo "$zsh_theme found."
        sed -i "s/^ZSH_THEME.*$/$zsh_theme/g" $home/.zshrc
    fi
fi
