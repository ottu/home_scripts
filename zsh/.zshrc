#th to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="fox"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how many often would you like to wait before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Oh my zsh auto upgrade
DISABLE_UPDATE_PROMPT="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...

# export LANG=ja_JP.UTF-8

HISTFILE=$HOME/.zsh-history
HISTSIZE=100000
SAVEHIST=100000

limit coredumpsize 102400

autoload -U compinit
compinit

autoload -U colors; colors

unsetopt promptcr
setopt correct
setopt re_match_pcre
setopt prompt_subst
setopt nobeep
setopt long_list_jobs
setopt list_types
setopt auto_resume
setopt auto_list
setopt hist_ignore_dups
setopt auto_pushd
setopt pushd_ignore_dups
setopt extended_glob
setopt auto_menu
setopt extended_history
setopt equals
setopt magic_equal_subst
setopt hist_ignore_dups     # ignore duplication command history list
setopt hist_verify
setopt share_history        # share command history data
setopt auto_param_slash
setopt mark_dirs
setopt list_packed
setopt noautoremoveslash

case ${OSTYPE} in
    darwin*)
        alias mvim='/Applications/MacVim.app/Contents/MacOS/mvim --remote-tab-silent "$@"'
        alias vim='/Applications/MacVim.app/Contents/MacOS/Vim "$@"'
        export PATH=/usr/local/opt/coreutils/libexec/gnubin:/usr/local/bin:/usr/local/opt/ruby/bin:$PATH
        ;;

    linux*)
        if [ -e /opt/android-sdk ]; then
           export PATH=/opt/android-sdk/tools:/opt/android-sdk/build-tools/19.1:/opt/android-sdk/platform-tools:$PATH
        fi
        ;;
esac

eval `dircolors`
export ZLS_COLORS=$LS_COLORS
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Vimで <Ctrl-S> を使えるようにする
stty -ixon -ixoff

alias grep='/usr/bin/grep --color=auto --exclude-dir=.cvs --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn'
unset GREP_OPTIONS

function peco-history-selection() {
    BUFFER=`fc -ln 1 | grep -v "^cd" | grep -v "^ls\( -al\)*$" | tac | awk '!a[$0]++' | peco`
    CURSOR=$#BUFFER
    zle reset-prompt
}
zle -N peco-history-selection
bindkey '^R' peco-history-selection

export PATH=$PATH:$HOME/.go/bin

source $HOME/.enhancd/src/init.sh

# Local Environment Settings...
if [ -e ~/.zshrc.local ]; then
    . ~/.zshrc.local
fi
